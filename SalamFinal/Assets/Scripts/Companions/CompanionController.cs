﻿/*Class:
 *  CompanionController
 *Purpose:
 *  Base class for companions.
 *  Contains the hero reference and allows following the hero.
 *  Animates the companion when walking.
 *  Speed is dependent on distance to hero (farther -> faster).
 */ 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entities
{
    public class CompanionController : MonoBehaviour
    {
        protected GameObject mHero;
        protected Vector3 mDirection;
        protected float mFollowDelay;
        protected float mSpeed;
        protected float mDistanceFromHero;

        protected Animator animator;

        // Use this for initialization
        protected virtual void Start()
        {
            mHero = GameObject.FindGameObjectWithTag("Hero");
            mFollowDelay = 1;
            mSpeed = 1f;
            mDistanceFromHero = 2.5f;

            animator = GetComponent<Animator>();
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            // Only follows if hero is visible
            if (mHero.GetComponent<HeroController>().GetVisible())
            {
                UpdateDirection();
                UpdatePosition();
            }
            else
            {
                animator.SetBool("isWalking", false);
            }
        }

        // Calculates direction of hero
        protected void UpdateDirection()
        {
            mDirection = mHero.transform.position - transform.position;
        }
        // Moves companion if farther than a set distance from hero
        protected void UpdatePosition()
        {
            if (mDirection.magnitude > mDistanceFromHero)
            {
                animator.SetBool("isWalking", true);
                animator.SetFloat("input_x", mDirection.x);
                animator.SetFloat("input_y", mDirection.y);
            }
            else
            {
                animator.SetBool("isWalking", false);
            }
        }
        void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.tag == "Hero")
            {
                other.gameObject.GetComponent<HeroController>().mParty.GetComponent<PartyHandler>().AddMember(this.gameObject);
            }

            if (other.gameObject.tag == "Hero" || other.gameObject.tag == "Companion")
            {
                animator.SetBool("isWalking", false);
            }

            if(other.gameObject.tag == "Enemy")
            {
                gameObject.SetActive(false);
            }
        }
    }
}