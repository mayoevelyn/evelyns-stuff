﻿/*Class:
 *  SlowCompanion
 *Purpose:
 *  This companion follows the hero slowly at a set speed.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entities
{
    public class SlowCompanion : CompanionController
    {

        // Use this for initialization
        protected override void Start()
        {
            base.Start();
            mSpeed = 3;
			   animator.SetBool("isWalking", false);
        }

        // Update is called once per frame
        protected override void Update()
        {
            if (mHero.GetComponent<HeroController>().GetVisible())
            {
                base.Update();
            }
            else
            {
                animator.SetBool("isWalking", false);
            }
        }
    }
}