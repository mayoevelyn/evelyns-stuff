﻿/*Class:
 *  WanderingCompanion
 *Purpose:
 *  This companion behaves as the fast companion does, except when within a certain
 *  radius of the hero, the companion wanders around and varying speeds.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entities
{
    public class WanderingCompanion : CompanionController
    {
        protected Vector2 mTempDir;
        protected float mTempSpeed;
        protected bool mStillWander;
        protected float mTime;
        protected float mStarted;

        // Use this for initialization
        protected override void Start()
        {
            base.Start();
            mFollowDelay = 1;
            mStillWander = false;
            mTime = 0;
            mStarted = 0;
        }

        // Update is called once per frame
        protected override void Update()
        {
            Wander();
        }

        // Wanders around within a circle around the hero. Speed and direction vary.
        // Upon leaving outer circle, quickly return to inner following circle then
        // continues to wander.
        void Wander()
        {
            // If outside outer circle, move to inner circle
            if (!mStillWander)
            {
                base.Update();
            }
            else
            {
                base.UpdateDirection();

                if (Time.time - mStarted > mTime)
                {
                    mTempDir = new Vector2(Random.value * 2 - 1, Random.value * 2 - 1);
                    mTempSpeed = Random.Range(1, 2);
                    mTempDir.Normalize();
                    mStarted = Time.time;
                    mTime = Random.value * 3;
                }
                else
                {
                    transform.Translate(mTempDir * Time.smoothDeltaTime * mTempSpeed);
                }
            }
            // Outer circle
            if (mDirection.magnitude >= 3)
            {
                mStillWander = false;
            }
            else if (mDirection.magnitude <= 1)
            {
                mStillWander = true;
            }
        }
    }
}