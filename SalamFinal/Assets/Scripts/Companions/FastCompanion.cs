﻿/*Class:
 *  FastCompanion
 *Purpose:
 *  This companion follows the hero quickly.
 *  Speed is based on distance to hero (farther -> faster).
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Entities
{
    public class FastCompanion : CompanionController
    {
        // Use this for initialization
        protected override void Start()
        {
            base.Start();
            mSpeed = 4;
        }

        // Update is called once per frame
        protected override void Update()
        {
            if (mHero.GetComponent<HeroController>().GetVisible())
            {
                base.Update();
            }
            else
            {
                animator.SetBool("isWalking", false);
            }
        }
    }
}