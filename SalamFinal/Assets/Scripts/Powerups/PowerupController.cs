﻿/*Class:
 *  PowerupController
 *Purpose:
 *  This is the base class for powerups.
 *  Allows for collection of powerups.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    protected int index;
    private GameObject Inventory;

    // Use this for initialization
    protected virtual void Start()
    {
        Inventory = GameObject.Find("Inventory");
    }

    // Update is called once per frame
    void Update()
    {

    }
    // When hero collides with a powerup, add powerup to inventory.
    void OnTriggerEnter2D(Collider2D other)
    {
        // Only care if hitting hero
        if (other.gameObject.tag == "Hero")
        {
            AddToInventory();
            Destroy(this.gameObject);
        }
    }

    protected virtual void AddToInventory()
    {
        Inventory.GetComponent<InventoryController>().IncreaseCount(index);
    }
}