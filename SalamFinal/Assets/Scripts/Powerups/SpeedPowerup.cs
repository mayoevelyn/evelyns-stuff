﻿/*Class:
 *  SpeedPowerup
 *Purpose:
 *  This powerup increases the speed of the hero by x2.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPowerup : PowerupController
{

	 // Use this for initialization
	 protected override void Start()
    {
        base.Start();
	 }
	 
	 // Update is called once per frame
	 void Update()
    {
		  
	 }

    // Adds powerup to inventory with base class.
    protected override void AddToInventory()
    {
        Debug.Log("Get powerup - speed");
        base.AddToInventory();
   }
}