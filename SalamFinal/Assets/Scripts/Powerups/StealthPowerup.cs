﻿/*Class:
 *  StealthPowerup
 *Purpose:
 *  This powerup makes the hero invisible to companions and enemies.
 *  This causes them to not detect/follow the hero at all.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealthPowerup : PowerupController
{

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        index = 1;
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Adds powerup to inventory using base class.
    protected override void AddToInventory()
    {
        Debug.Log("Get powerup - stealth");
        base.AddToInventory();
    }
}