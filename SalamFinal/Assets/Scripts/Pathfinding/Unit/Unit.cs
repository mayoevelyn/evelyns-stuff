﻿/*Class:
 *  Unit
 *Purpose:
 *  This class is placed onto the game object that needs pathfinding.
 *  This is the base class for the abstracted unit classes that control companions and enemies.
 */

using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour
{
    public Transform target;
    public bool shouldMove;
    public float speed;

    Vector2[] path;
    int targetIndex;

    protected virtual void Start()
    {
        shouldMove = true;
        StartCoroutine(RefreshPath());
    }

    void OnEnable()
    {
        shouldMove = true;
        StartCoroutine(RefreshPath());
    }

    IEnumerator RefreshPath()
    {
        Vector2 targetPositionOld = (Vector2)target.position + Vector2.up; // ensure != to target.position initially
        //print("Old target position: " + targetPositionOld);

        while (true)
        {
            if (targetPositionOld != (Vector2)target.position && shouldMove)
            {
                targetPositionOld = (Vector2)target.position;

                path = Pathfinding.RequestPath(transform.position, target.position);
                StopCoroutine("FollowPath");
                StartCoroutine("FollowPath");
            }
            else if (targetPositionOld != (Vector2)target.position && !shouldMove)
            {
                StopCoroutine("FollowPath");
            }

            yield return new WaitForSeconds(.25f);
        }
    }

    IEnumerator FollowPath()
    {
        if (path.Length > 0)
        {
            targetIndex = 0;
            Vector2 currentWaypoint = path[0];

            while (true)
            {
                if ((Vector2)transform.position == currentWaypoint)
                {
                    targetIndex++;

                    if (targetIndex >= path.Length)
                    {
                        yield break;
                    }
                    currentWaypoint = path[targetIndex];
                }

                transform.position = Vector2.MoveTowards(transform.position, currentWaypoint, speed * Time.smoothDeltaTime);

                yield return null;

            }
        }
    }

    float FindDistance(Transform other)
    {
        float dist = (other.position - transform.position).magnitude;
        return dist;
    }

    public void OnDrawGizmos()
    {
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                //Gizmos.color = Color.black;
                Gizmos.DrawCube((Vector3)path[i], Vector3.one * .5f);

                if (i == targetIndex)
                {
                    Gizmos.DrawLine(transform.position, path[i]);
                }
                else
                {
                    Gizmos.DrawLine(path[i - 1], path[i]);
                }
            }
        }
    }

    void OnTriggerExit2D()
    {
        shouldMove = true;
    }
}