﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanionUnit : Unit
{
	 // Use this for initialization
	 new void Start ()
    {
        base.Start();
        shouldMove = true;
	 }
	 
	 // Update is called once per frame
	 void Update ()
    {
		  if ((GameObject.FindGameObjectWithTag("Hero").transform.position - transform.position).magnitude > 2)
        {
            shouldMove = true;
        }
	 }
    // Stop moving if close to other companion
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Companion")
        {
            shouldMove = false;
        }
    }
}