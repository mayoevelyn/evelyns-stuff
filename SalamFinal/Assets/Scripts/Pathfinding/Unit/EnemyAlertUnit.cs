﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAlertUnit : Unit
{
    public GameObject[] pathNodes;
    Vector2[] mPatrolPath;
    int index;
    // Use this for initialization
    new void Start()
    {
        mPatrolPath = new Vector2[pathNodes.Length];
        for (int i = 0; i < pathNodes.Length; i++)
        {
            mPatrolPath[i] = new Vector2(pathNodes[i].transform.position.x, pathNodes[i].transform.position.y);
        }

        index = 0;
        Update();
        base.Start();
        shouldMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        target.position = mPatrolPath[index];
        if ((new Vector2(transform.position.x, transform.position.y) - mPatrolPath[index]).magnitude < 2)
        {
            index++;
            if (index >= mPatrolPath.Length)
            {
                index = 0;
            }
        }
    }
}