﻿/*Class:
 *  KeyItem
 *Purpose:
 *  This item can open locked doors. Consumed on use.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyItem : ItemController
{

	 // Use this for initialization
	 protected override void Start ()
    {
        base.Start();
        index = 2;
	 }
	 
	 // Update is called once per frame
	 void Update ()
    {
		  
	 }

    // Adds key to inventory using base class.
   protected override void AddToInventory()
   {
      Debug.Log("Get item - key");
      base.AddToInventory();
   }
}