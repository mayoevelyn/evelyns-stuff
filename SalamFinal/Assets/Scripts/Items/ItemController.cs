﻿/*Class:
 *  ItemController
 *Purpose:
 *  Base class for items.
 *  Allows for adding to inventory on hero collision.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    protected int index;
    private GameObject Inventory;

    // Use this for initialization
    protected virtual void Start()
    {
        Inventory = GameObject.Find("Inventory");
    }

    // Update is called once per frame
    void Update()
    {

    }

    // If hero collects (collides with), add to inventory and kill gameobject.
    void OnTriggerEnter2D(Collider2D other)
    {
        // Only care if hitting hero
        if (other.gameObject.tag == "Hero")
        {
            AddToInventory();
            Destroy(this.gameObject);
        }
    }
    // Increases count of proper object in inventory.
    protected virtual void AddToInventory()
    {
        Inventory.GetComponent<InventoryController>().IncreaseCount(index);
    }
}