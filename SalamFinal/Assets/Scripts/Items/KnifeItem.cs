﻿/*Class:
 *  KnifeItem
 *Purpose:
 *  This item is a knife that can be used in some utility scenarios.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeItem : ItemController
{

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        index = 3;
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Adds knife to inventory using base class.
   protected override void AddToInventory()
   {
      Debug.Log("Get item - knife");
      base.AddToInventory();
   }
}