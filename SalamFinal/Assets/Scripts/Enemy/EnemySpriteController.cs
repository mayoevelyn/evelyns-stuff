﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpriteController : MonoBehaviour
{
    private Transform mFOVTransform;
    private Animator mAnimator;
    private float mFOVAngle;

    // Use this for initialization
    void Start()
    {
        mFOVTransform = gameObject.transform.GetChild(1).gameObject.transform;
        mAnimator = gameObject.transform.GetChild(0).GetComponent<Animator>();
        mFOVAngle = 0;

        mAnimator.SetBool("isWalking", true);
    }

    // Update is called once per frame
    void Update()
    {
        mFOVAngle = Vector2.Angle(Vector2.right, mFOVTransform.up.normalized);
        if(mFOVTransform.up.y < 0)
        {
            mFOVAngle = 360 - mFOVAngle;
        }
        if(mFOVAngle >= 45 && mFOVAngle <= 135)         // Walking up
        {
            //Debug.Log("Up");
            mAnimator.SetFloat("input_x", 0);
            mAnimator.SetFloat("input_y", 1);
        }
        else if(mFOVAngle > 135 && mFOVAngle < 225)     // Walking left
        {
            //Debug.Log("Left");
            mAnimator.SetFloat("input_x", -1);
            mAnimator.SetFloat("input_y", 0);
        }
        else if(mFOVAngle >= 225 && mFOVAngle <= 315)   // Walking right
        {
            //Debug.Log("Down");
            mAnimator.SetFloat("input_x", 0);
            mAnimator.SetFloat("input_y", -1);
        }
        else
        {
            //Debug.Log("Right");
            mAnimator.SetFloat("input_x", 1);
            mAnimator.SetFloat("input_y", 0);
        }
    }
}