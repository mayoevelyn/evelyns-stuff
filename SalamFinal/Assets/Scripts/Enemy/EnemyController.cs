﻿/*Class:
 *  EnemyController
 *Purpose:
 *  Handles enemy states, including movement, detection, and chasing.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entities
{
    public class EnemyController : MonoBehaviour
    {
        private enum EnemyState
        {
            Patrol,
            Alert,
            Chase
        };

        private GameObject mHero;
        private GameObject mHitEntity;
        public GameObject mTarget;
        private GameObject mEnemySprite;
        private GameObject mEnemyFOV;

        private EnemyState mCurrentState;
        private Vector3 mHitDir;
        private Vector3 mTargetDir;
        private float mChaseTime;
        private float mTimeSeen;

        private Animator mAnimator;
        private EnemyPatrolUnit mPatrolScript;
        private EnemyAlertUnit mAlertScript;
        private EnemyChaseUnit mChaseScript;

        // Fetches the hero object and initializes variables.
        void Start()
        {
            mHero = GameObject.FindGameObjectWithTag("Hero");
            mHitEntity = null;
            //mTarget = GameObject.Find("EnemyTarget");

            mHitDir = Vector3.zero;
            mTargetDir = Vector3.zero;
            mCurrentState = EnemyState.Patrol;
            mChaseTime = 3;
            mTimeSeen = 0;

            mEnemySprite = gameObject.transform.GetChild(0).gameObject;
            mEnemyFOV = gameObject.transform.GetChild(1).gameObject;

            mAnimator = mEnemySprite.GetComponent<Animator>();

            mPatrolScript = GetComponent<EnemyPatrolUnit>();
            mAlertScript = GetComponent<EnemyAlertUnit>();
            mChaseScript = GetComponent<EnemyChaseUnit>();
        }

        // Update is called once per frame
        void Update()
        {
            UpdateState();
            Execute();
        }

        // Stops enemy chasing if timer is up.
        void UpdateState()
        {
            if (mCurrentState == EnemyState.Chase && Time.time - mTimeSeen >= mChaseTime)
            {
                mCurrentState = EnemyState.Alert;
            }
        }

        // Executes the current state.
        void Execute()
        {
            switch (mCurrentState)
            {
                case EnemyState.Patrol:     // Unalert patrol
                    mPatrolScript.enabled = true;
                    mAlertScript.StopAllCoroutines();
                    mAlertScript.enabled = false;
                    mChaseScript.StopAllCoroutines();
                    mChaseScript.enabled = false;

                    mTargetDir = mTarget.transform.position - transform.position;
                    mEnemyFOV.transform.up = mTargetDir;
                    break;
                case EnemyState.Alert:      // Alert patrol
                    mPatrolScript.StopAllCoroutines();
                    mPatrolScript.enabled = false;
                    mAlertScript.enabled = true;
                    mChaseScript.StopAllCoroutines();
                    mChaseScript.enabled = false;
                    
                    mTargetDir = mTarget.transform.position - transform.position;
                    mEnemyFOV.transform.up = mTargetDir;
                    break;
                case EnemyState.Chase:      // Active chase
                    mPatrolScript.StopAllCoroutines();
                    mPatrolScript.enabled = false;
                    mAlertScript.StopAllCoroutines();
                    mAlertScript.enabled = false;
                    mChaseScript.enabled = true;

                    mHitDir = mHitEntity.transform.position - transform.position;
                    //Debug.Log("mHeroDir: " + mHitDir);
                    mEnemyFOV.transform.up = mHitDir;
                    break;
                default:
                    break;
            }
        }
        // When hero is seen by enemy (inside detection area).
        void OnTriggerStay2D(Collider2D other)
        {
            // Calculate and raycast only if object seen is hero or companion and hero/party is visible
            if (mHero.GetComponent<HeroController>().GetVisible() &&
                (other.gameObject.tag == "Hero" || other.gameObject.tag == "Companion"))
            {
                // Direction of other
                Vector2 hitDir = other.transform.position - transform.position;
                // Distance to other
                float hitDist = hitDir.magnitude;
                //Debug.Log("test");
                // Raycast to see if view to hero or companion is obstructed
                RaycastHit2D raycastHit = Physics2D.Raycast(transform.position, hitDir, hitDist * 2);
                if (raycastHit.transform != null &&
                    (raycastHit.transform.tag == "Hero" || raycastHit.transform.tag == "Companion"))
                {
                    mTimeSeen = Time.time;
                    mCurrentState = EnemyState.Chase;
                    mHitEntity = other.gameObject;
                }
            }
        }
    }
}