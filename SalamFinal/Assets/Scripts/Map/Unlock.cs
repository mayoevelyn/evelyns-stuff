﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class Unlock : MonoBehaviour {
    BoxCollider2D collider;

	// Use this for initialization
	void Start () {
        collider = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [YarnCommand("unlock")]
    public void UnlockDoor()
    {
        collider.isTrigger = true;
    }
}
