﻿using UnityEngine;
using System.Collections;

// set keepPressed to true if you need the button to have an item on top of it
// default keyChar is b
// default keyPressed is true

public class FloorButton : MonoBehaviour
{
    public string keyChar;
    public bool keepPressed = true;
    public bool isPressed = false;
    public Sprite notPressed;
    public Sprite pressed;
    public FloorButtonManager manag;
    SpriteRenderer rend;
    
    // Use this for initialization
    void Start()
    {
        if (keyChar.Equals(""))
            keyChar = "b";
        rend = GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isPressed == false)
        {
            isPressed = true;
            rend.sprite = pressed;
            manag.currentInput += keyChar;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (keepPressed == true)
        {
            isPressed = false;
            rend.sprite = notPressed;
            manag.currentInput = manag.currentInput.Substring(0, manag.currentInput.Length-1);
        }
    }

    public void resetButton()
    {
        isPressed = false;
        rend.sprite = notPressed;
    }
}
