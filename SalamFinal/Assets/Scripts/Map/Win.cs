﻿/*Class:
 *  Win
 *Purpose:
 *  This class transitions the game into the Win ending (scene).
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Hero")
        {
            SceneManager.LoadScene(3);
        }
    }
}
