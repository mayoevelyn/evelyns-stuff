﻿/*Class:
 *  Warp
 *Purpose:
 *  When the hero contacts a door, warps the hero to the proper room.
 */

using UnityEngine;
using System.Collections;

namespace Entities
{
    public class Warp : MonoBehaviour
    {
        public Transform warpTarget;
        HeroController script;

        // Use this for initialization
        void Start()
        {
            script = GameObject.FindGameObjectWithTag("Hero").GetComponent<HeroController>();
        }

        IEnumerator OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Hero")
            {
                script.enabled = false;
                ScreenFader sf = GameObject.FindGameObjectWithTag("Fader").GetComponent<ScreenFader>();
                yield return StartCoroutine(sf.FadeToBlack());

                collision.gameObject.transform.position = warpTarget.position;
                collision.gameObject.GetComponent<HeroController>().mParty.gameObject.
                    GetComponent<PartyHandler>().WarpParty(warpTarget.position);
                Camera.main.transform.position = new Vector3(warpTarget.position.x, warpTarget.position.y, -10);
                
                yield return StartCoroutine(sf.FadeToClear());
                script.enabled = true;
            }
        }
    }
}