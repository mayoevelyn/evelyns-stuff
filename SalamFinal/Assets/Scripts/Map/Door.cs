﻿using UnityEngine;
using System.Collections;

// door closed is first child
// door open is second child

public class Door : MonoBehaviour
{
    public BoxCollider2D col;
    public SpriteRenderer[] rends;

    // Use this for initialization
    void Start()
    {
        col = GetComponentInChildren(typeof(BoxCollider2D)) as BoxCollider2D;
        rends = GetComponentsInChildren<SpriteRenderer>(true);
    }    
}