﻿/*Class:
 *  GameController
 *Purpose:
 *  Handles general game functions.
 *  Currently empty due to no runtime spawning needs.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;
using Yarn.Unity.Example;

public class GameController : MonoBehaviour
{
    //private GameObject mHero;

    new AudioSource audio;
    // Use this for initialization
    void Start ()
    {

        //mHero = GameObject.FindGameObjectWithTag("Hero");
        //FindObjectOfType<DialogueRunner>().StartDialogue("Sally");
        audio = GetComponent<AudioSource>();
        audio.Play();
     }
	 
	 // Update is called once per frame
	 void Update ()
    {

    }
}