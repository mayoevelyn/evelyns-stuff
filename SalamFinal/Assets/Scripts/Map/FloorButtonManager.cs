﻿using UnityEngine;
using System.Collections;

// Make buttons the child of its manager
// Fill up the correct input
// Attach the door to open when solved

public class FloorButtonManager : MonoBehaviour
{
    public string correctInput = "";
    Component[] buttons;
    Door door;

    bool solved = false;
    public string currentInput;

    // Use this for initialization
    void Start()
    {
        door = GetComponentInChildren(typeof(Door)) as Door;
        buttons = GetComponentsInChildren(typeof(FloorButton));

        // if correctInput is not specified in unity, set it "b"s as long as buttons array
        if (correctInput.Equals(""))
        {
            string defaultInput = "";
            for (int i = 0; i < buttons.Length; i++)
            {
                defaultInput += "b";
            }
            correctInput = defaultInput;
        }
        foreach (FloorButton button in buttons)
        {
            button.manag = this;
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (solved == false && currentInput.Length == correctInput.Length)
        {
            if (currentInput.Equals(correctInput))
            {
                solved = true;
                door.col.enabled = false;
                foreach (SpriteRenderer rend in door.rends)
                {
                    rend.enabled = !rend.enabled;
                }
            }
            else
                resetButtons();
        }
    }

    void resetButtons()
    {
        foreach (FloorButton button in buttons)
        {
            button.resetButton();
        }
        currentInput = "";
    }
}
