﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entities
{
    public class HideableController : MonoBehaviour
    {
        private GameObject mHero;
        bool isHiding = false;
        public Sprite openCrate;
        public Sprite closedCrate;
        SpriteRenderer rend;
        HeroController script;

        // Use this for initialization
        void Start()
        {
            mHero = GameObject.FindGameObjectWithTag("Hero");
            script = mHero.GetComponent<HeroController>();
            rend = GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
        }

        // Update is called once per frame
        void Update()
        {
            //if ((mHero.transform.position - transform.position).magnitude < 0.2)
            //{
            //    if (Input.GetKeyDown(KeyCode.Q))
            //    {
            //        mHero.GetComponent<HeroController>().mParty.GetComponent<PartyHandler>().StoreCompanion();
            //    }
            //    else if (Input.GetKeyDown(KeyCode.E))
            //    {
            //        mHero.GetComponent<HeroController>().mParty.GetComponent<PartyHandler>().RetrieveCompanion();
            //    }
            //}

            if ((mHero.transform.position - transform.position).magnitude < 0.2)
            {
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    if (isHiding == false)
                    {
                        hideCompanions();
                    }
                    else
                    {
                        getCompanions();
                    }
                }
            }
        }

        IEnumerator hideCompanions()
        {
            // fade to black
            script.enabled = false;
            ScreenFader sf = GameObject.FindGameObjectWithTag("Fader").GetComponent<ScreenFader>();
            yield return StartCoroutine(sf.FadeToBlack());

            // store all companions
            mHero.GetComponent<HeroController>().mParty.GetComponent<PartyHandler>().StoreCompanion();
            // update sprite
            rend.sprite = closedCrate;
            // update isHiding
            isHiding = true;

            // fade to clear
            yield return StartCoroutine(sf.FadeToClear());
            script.enabled = true;
        }

        IEnumerator getCompanions()
        {
            // fade to black
            script.enabled = false;
            ScreenFader sf = GameObject.FindGameObjectWithTag("Fader").GetComponent<ScreenFader>();
            yield return StartCoroutine(sf.FadeToBlack());

            // store all companions
            mHero.GetComponent<HeroController>().mParty.GetComponent<PartyHandler>().RetrieveCompanions();
            // update sprite
            rend.sprite = openCrate;
            // update isHiding
            isHiding = false;

            // fade to clear
            yield return StartCoroutine(sf.FadeToClear());
            script.enabled = true;
        }
    }
}