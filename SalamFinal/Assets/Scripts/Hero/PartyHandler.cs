﻿/*Class:
 *  PartyHandler
 *Purpose:
 *  This class manages the hero's party.
 *  Party currently holds up to two members and warps the party with the hero.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entities
{
    public class PartyHandler : MonoBehaviour
    {
        private GameObject[] PartyList;
        private int PartySize;

        // Use this for initialization
        void Start()
        {
            PartyList = new GameObject[3];
            PartySize = 0;
        }

        // Adds party member to list
        public void AddMember(GameObject toAdd)
        {
            if(toAdd.tag == "Companion" && !AlreadyAdded(toAdd))
            {
                Debug.Log("Party member added");
                PartyList[PartySize] = toAdd;
                PartySize++;
            }
        }
        // Checks if the companion collided with is already in the list.
        bool AlreadyAdded(GameObject toCheck)
        {
            bool alreadyAdded = false;
            for (int i = 0; i < PartySize; i++)
            {
                if(PartyList[i] == toCheck)
                {
                    alreadyAdded = true;
                }
            }
            return alreadyAdded;
        }
        // Warps the whole party with the hero.
        public void WarpParty(Vector3 warpTarget)
        {
            for (int i = 0; i < PartySize; i++)
            {
                PartyList[i].transform.position = warpTarget;
            }
        }

        // Hides last member in party
        public void StoreCompanion()
        {
            if(PartySize - 1 >= 0)
            {
                PartySize--;
                PartyList[PartySize].SetActive(false);
            }
        }

        // Hides all members in the party
        public void StoreCompanions()
        {
            foreach (GameObject partyMember in PartyList)
            {
                partyMember.SetActive(false);
                PartySize -= PartyList.Length;
            }
        }

        // Unhides most recently hidden member in party
        public void RetrieveCompanion()
        {
            if(PartySize <= PartyList.Length && PartyList[PartySize] != null)
            {
                PartyList[PartySize].SetActive(true);
                PartySize++;
            }
        }

        // Unhides all hidden members in party
        public void RetrieveCompanions()
        {
            foreach (GameObject partyMember in PartyList)
            {
                partyMember.SetActive(true);
                PartySize += PartyList.Length;
            }
        }
    }
}