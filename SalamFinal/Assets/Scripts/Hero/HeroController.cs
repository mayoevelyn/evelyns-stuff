﻿/*Class:
 *  HeroController
 *Purpose:
 *  Handles control of hero.
 *  This includes powerup activation and hero movement/animation.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Yarn.Unity;
using Yarn.Unity.Example;

namespace Entities
{
    public class HeroController : MonoBehaviour
    {
        // Tracks powerup state of hero.
        private enum HeroState
        {
            Normal,
            Speed,
            Stealth
        }

        public GameObject mParty;

        private bool mDebugMode;
        private bool mIsVisible;
        public bool GetVisible()
        {
            return mIsVisible;
        }

        public float mMovementSpeed;  // Active movement speed
        public float mPowerupTime;  // Time powerup is active

        private HeroState mState;
        private float mTime;        // Timestamp the powerup was activated
        private float mBaseSpeed;     // Base movement speed

        private Animator animator;
        private GameObject Inventory;

        public float interactionRadius = 2.0f;  // distance to NPC

        // Use this for initialization
        void Start()
        {
            mParty = GameObject.Find("PartyHandler");

            // Debug off, visible to entities, no speed modifiers
            mDebugMode = false;
            mIsVisible = true;
            mBaseSpeed = mMovementSpeed;

            animator = GetComponent<Animator>();
            Inventory = GameObject.Find("Inventory");
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                mDebugMode = !mDebugMode;
            }

            // Debug mode: pass through collidables, be invisible to entities, and move faster
            if (mDebugMode)
            {
                GetComponent<Collider2D>().enabled = false;
                mIsVisible = false;
                mMovementSpeed = mBaseSpeed * 10;
            }
            else
            {
                GetComponent<Collider2D>().enabled = true;
                mIsVisible = true;
                mMovementSpeed = mBaseSpeed;
            }
        }

        // Update is called once per frame
        void FixedUpdate()
        {

            //if (FindObjectOfType<DialogueRunner>().isDialogueRunning == true)
            //{
            //    return;
            //}

            // check for NPC
            if (Input.GetKeyDown(KeyCode.Return)) CheckForNearbyNPC();

            // Z for speed, X for stealth
            if (Input.GetKeyUp(KeyCode.Z)) UsePowerUp(0);
            if (Input.GetKeyUp(KeyCode.X)) UsePowerUp(1);

            Vector3 movementVector = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            // Walking animation plays if hero is moving
            if (movementVector != Vector3.zero)
            {
                animator.SetBool("isWalking", true);
                animator.SetFloat("input_y", movementVector.y);
                animator.SetFloat("input_x", movementVector.x);
            }
            else
            {
                animator.SetBool("isWalking", false);
            }

            transform.position += movementVector * Time.smoothDeltaTime * mMovementSpeed;
            CheckPowerupStatus();
        }

        // Activates proper powerup changes. Times powerup states.
        void CheckPowerupStatus()
        {
            switch (mState)
            {
                case HeroState.Normal:  // Base
                    mIsVisible = true;
                    GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
                    mMovementSpeed = mBaseSpeed;
                    GetComponent<SpriteRenderer>().enabled = true;
                    break;
                case HeroState.Speed:   // Doubles speed
                    if (Time.time - mTime > mPowerupTime)
                    {
                        mState = HeroState.Normal;
                    }
                    else
                    {
                        GetComponent<SpriteRenderer>().color = new Color(1f, 0.5f, 0.5f, 1f);
                        mMovementSpeed = mBaseSpeed * 3;
                    }
                    break;
                case HeroState.Stealth: // Hero invisible to companions, enemies - reduces movement speed
                    Debug.Log("Check");
                    if (Time.time - mTime > mPowerupTime)
                    {
                        mState = HeroState.Normal;
                    }
                    else
                    {
                        mIsVisible = false;
                        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);
                        mMovementSpeed = (int)(mBaseSpeed * 0.8);
                    }
                    break;
                default:
                    break;
            }
        }
        // When a powerup is activates, sets timer and hero state.
        public void UpdateState(int newState)
        {
            switch (newState)
            {
                case 0:     // Speed powerup
                    mState = HeroState.Speed;
                    mTime = Time.time;
                    break;
                case 1:     // Stealth powerup
                    mState = HeroState.Stealth;
                    mTime = Time.time;
                    break;
                default:
                    break;
            }
        }
        // Activates the power with UpdateState() if we have at least one in inventory
        void UsePowerUp(int index)
        {
            bool success = Inventory.GetComponent<InventoryController>().DecreaseCount(index);
            if (success)
            {
                mTime = Time.time;
                if (index == 0)
                    UpdateState(0);
                else if (index == 1)
                    UpdateState(1);
            }
        }

        public void CheckForNearbyNPC()
        {
            // Find all DialogueParticipants, and filter them to
            // those that have a Yarn start node and are in range; 
            // then start a conversation with the first one
            var allParticipants = new List<NPC>(FindObjectsOfType<NPC>());
            var target = allParticipants.Find(delegate (NPC p) {
                return string.IsNullOrEmpty(p.talkToNode) == false && // has a conversation node?
                (p.transform.position - this.transform.position)// is in range?
                .magnitude <= interactionRadius;
            });
            if (target != null)
            {
                // Kick off the dialogue at this node.
                FindObjectOfType<DialogueRunner>().StartDialogue(target.talkToNode);
            }
        }
    }
}