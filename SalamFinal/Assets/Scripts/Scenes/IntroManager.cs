﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Entities;


public class IntroManager : MonoBehaviour {

     
    public AudioSource audio;
    public static float FadeTime = 2f;
    public static float TextTime = 1f;

    string titleComplete;

    void Awake()
    {
        audio = GetComponent<AudioSource>();
        audio.Play();
    }
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public IEnumerator FadeOut()
    {
        float startVolume = audio.volume;

        while (audio.volume > 0)
        {
            audio.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audio.Stop();
        audio.volume = startVolume;
    }

}
