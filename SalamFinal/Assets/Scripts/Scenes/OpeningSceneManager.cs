﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class OpeningSceneManager : MonoBehaviour
{
    public AudioClip door;
    public AudioClip woodBreak;
    public AudioSource audio;

    public string nextMap;

    public Text textBox;
    string[] page;
    int currentPage;

    IEnumerator AnimateText(string strComplete)
    {

        textBox.text = "";
        for (int i = 0; i < strComplete.Length; i++)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                textBox.text = strComplete.Substring(0, strComplete.Length - 1);
                break;
            }
            textBox.text = strComplete.Substring(0, i);
            if (strComplete[i] == '\n')
                yield return new WaitForSeconds(0.5f);
            else
                yield return new WaitForSeconds(0.01f);
        }
        yield return new WaitForSeconds(1f);
        textBox.text += "\n\n\nPress enter to continue...";
        yield return StartCoroutine(WaitForEnterUp());
        textBox.text = "";
        currentPage++;
        if (currentPage < page.Length)
        {
            StartCoroutine(playSound());
            StartCoroutine(AnimateText(page[currentPage]));
        }
        else LoadMap();
    }

    IEnumerator WaitForEnterUp()
    {
        while (!Input.GetKeyUp(KeyCode.Return))
            yield return null;
    }

    IEnumerator playSound()
    {
        if (currentPage == 2)
        {
            audio.PlayOneShot(door, 1);
            yield return new WaitForSeconds(.5f);
            audio.PlayOneShot(door, 1);
        }
        else if (currentPage == 4)
        {
            audio.PlayOneShot(woodBreak, 1);
        }
    }
    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.Play();
        currentPage = 0;
        page = new string[7];
        page[0] = "How many years has it been since war broke out?\n\n" +
              "I can no longer keep track of the days since you have been away, leaving me here with our children.\n\n" +
              "It's a struggle to support them alone, especially since the essentials are so scarce. \n\n" +
              "Dear, please make it back safely...!\n\n" +
              "Our children need you!.";
        page[1] = "I've heard... that the enemy will reach our town in a day or so, but I cannot be sure.\n\n" +
              "I'm planning to leave before then, taking our children with me.\n\n" +
              "The desert is unkind. Outside this oasis, there is hardly any water or food to be found.\n\n" +
              "The days are sweltering and the nights are unbearably cold.\n\n" +
              "But I would rather face the challenges of the desert than leave our fates in the hands of those who've killed so many of us...\n\n" +
              "I only hope that the children can make the journey..";
        page[2] = "*knock knock*\n\n\"!!!\"\n\nSoldier 1: \"\"Is there anyone in this house!?\"\"\n\n" +
              "Soldier 2: \"\"Probably. I saw a light earlier. Break down the door!\"\"\n\n.";
        page[3] = "Enemy soldiers!?\n\nWhen did they invade? They weren't supposed to reach here yet!.";
        page[4] = "*crash*\n\nSoldiers: \"\"Hands up!\"\"\n\n" + "\"Mama, who are these people?\"\n\n\"I'm scared...\"\n\n" +
              "\"Hush, my child, I'm here--\"\n\n Soldier 1: \"\"Be quiet! Otherwise I'll silence you myself!\"\".";
        page[5] = "I can't understand what they're saying. What do they want from us? We have nothing!\n\n" +
              "Soldier 1: \"\"Load them into the truck! We can't let a single one escape!\"\"\n\nSoldiers: \"\"Yessir!\"\".";
        page[6] = "[The woman and her children are taken away...].";
        StartCoroutine(AnimateText(page[currentPage]));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            LoadMap();
    }

    void LoadMap()
    {
        SceneManager.LoadScene(nextMap);
    }
}
