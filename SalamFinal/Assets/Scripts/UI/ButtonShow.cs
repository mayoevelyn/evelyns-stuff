﻿using UnityEngine;

public class ButtonShow : ButtonClick
{

    public GameObject toActivate;

    protected override void TaskOnClick()
    {
        if (toActivate.activeInHierarchy)
            toActivate.SetActive(false);
        else toActivate.SetActive(true);
    }
}
