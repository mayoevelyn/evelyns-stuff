﻿/*Class:
 *  InventoryController
 *Purpose:
 *  This class controls the flow of the hero's inventory.
 *  Tracks the number and types of items the hero can have.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Yarn.Unity;

public class InventoryController : MonoBehaviour
{
   int TypesOfItems;
   int[] ItemCounts;
   public Text[] TextDisplays;
   public Text ItemGetDisplay;
   float ItemGetTime;
   const float ItemGetDisplayTime = 1f;

   // Use this for initialization
   void Start()
   {
        ItemGetDisplay.enabled = false;
        TypesOfItems = TextDisplays.Length;
        ItemCounts = new int[TypesOfItems];

        UpdateCount();
   }

   void Update()
   {
        if (ItemGetDisplay.enabled == true && Time.time - ItemGetTime >= ItemGetDisplayTime)
        {
            ItemGetDisplay.enabled = false;
        }
   }

    void UpdateCount()
    {
        for (int i = 0; i < TypesOfItems; i++)
        {
            TextDisplays[i].text = ItemCounts[i] + "";
        }
    }

   public void IncreaseCount(int index)
   {
      if (index >= 0 && index < TypesOfItems)
      {
         ItemCounts[index]++;
         ItemGetTime = Time.time;
         string itemName;
         switch (index)
         {
            case 0:
               itemName = "Speed potion";
               break;
            case 1:
               itemName = "Stealth potion";
               break;
            case 2:
               itemName = "Key";
               break;
            case 3:
               itemName = "Knife";
               break;
            default:
               itemName = "";
               break;
         }
         ItemGetDisplay.text = itemName + " obtained.";
         ItemGetDisplay.enabled = true;
         UpdateCount();
      }
   }

   // decrease count of item, returns false if count of that item is 0
   public bool DecreaseCount(int index)
   {
      if (HasItem(index))
      {
         ItemCounts[index]--;
         UpdateCount();
         return true;
      }
      return false;
   }

   public bool HasItem(int index)
   {
      if ((index >= 0 && index < TypesOfItems) && ItemCounts[index] > 0) return true;
      return false;
   }

    [YarnCommand("checkkey")]
    public void CheckKey()
    {
        if (ItemCounts[2] > 0)
            FindObjectOfType<DialogueRunner>().StartDialogue("Key.Use");
    }

    [YarnCommand("usekey")]
    public void UseKey()
    {
        DecreaseCount(2);
        Debug.Log("Key used.");
    }
}
