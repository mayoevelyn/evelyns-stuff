﻿using UnityEngine;

public class ButtonQuit : ButtonClick
{

    protected override void TaskOnClick()
    {
        Application.Quit();
    }
}