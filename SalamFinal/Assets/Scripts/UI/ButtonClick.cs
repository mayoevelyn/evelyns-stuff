﻿/*Class:
 *  ButtonClick
 *Purpose:
 *  Resets the game to the beginning when the reset button is hit (win/lose screen).
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonClick : MonoBehaviour
{
   public Button yourButton;
   public string SceneToLoad;

   void Start()
   {
      Button btn = yourButton.GetComponent<Button>();
      btn.onClick.AddListener(TaskOnClick);
   }

   protected virtual void TaskOnClick()
   {
      SceneManager.LoadScene(SceneToLoad);
   }
}